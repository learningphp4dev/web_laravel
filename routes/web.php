<?php

use App\user_profile;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/homeyyy', function () {
//     return view('home');
// });

Route::get('/home','demo@index');



// CRUD

// show login form from controller nothing else
Route::get('/','user_profile_cont@index');


// insert link
Route::post('details_send', 'user_profile_cont@store');




//read link
Route::get('fetch','user_profile_cont@show')->middleware('login_check');

// Delete
Route::get('delete/{id}','user_profile_cont@delete');

// Edit
Route::get('Edit/{id}','user_profile_cont@edit');


Route::post('edit_details', 'user_profile_cont@update_fn');

// Login 
Route::get('login', 'user_profile_cont@login_form');



Route::post('login_fn', 'user_profile_cont@login');


Route::get('logout','user_profile_cont@logout');
// Route::get('jaspreet', 'demo@index');

// Show user dashboard
Route::get('dashboard', 'user_profile_cont@user')->middleware('login_check');

// check email exist or not
Route::get('email_check','user_profile_cont@check_email');

// Google Authentication Route
Route::get('/redirect', 'SocialAuthGoogleControllerController@redirect');
Route::get('/google_callback', 'SocialAuthGoogleControllerController@callback');

// registeration complte route
Route::get('register_complete_page',function(){
    return view('home');
});

Route::post('register_complete','user_profile_cont@register_complete');