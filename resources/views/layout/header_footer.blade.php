<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('Jquery/jquery-3.4.1.js')}}"></script>
</head>

<body>

    @section('navbar')
    <nav class="navbar bg-light text-dark navbar-expand-lg">
        <div class="nav-brand">Avatar</div>
        <ul class="navbar-nav mr-auto">
            @if(Session::has('logged_in'))
            <li class="nav-item"><a href="{{url('logout')}}" class="nav-link">Logout</a></li>
            <li class="nav-item"><a href="{{url('dashboard')}}" class="nav-link">My Account </a></li>
            <li class="nav-item"><a href="{{url('fetch')}}" class="nav-link">Show all records </a></li>
            @else
            <li class="nav-item"><a href="{{url('login')}}" class="nav-link">Login</a></li>
            <li class="nav-item"><a href="{{url('/')}}" class="nav-link">Register</a></li>
            @endif
        </ul>
    </nav>

    @show

    @yield('main_content')

    @section('footer')
    <div class="footer">
        <h1 class="text-center bg-dark text-white">Footer Section</h1>
    </div>
    @show
</body>

</html>