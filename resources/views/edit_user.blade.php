@extends('layout.header_footer')

@section('navbar')

@parent

@endsection

@section('main_content')

<div class="container">
    <div class="row">
        <div class="col-md-6 mx-auto bg-dark p-5 text-white">
            <form action="{{url('edit_details')}}" method="post">
                @csrf()
                <label for="username">Username</label>
                <input type="text" name="username" class="form-control" value="{{$user_data['username']}}" placeholder="Enter username">

                <label for="email">Email</label>
                <input type="email" name="email" class="form-control" value="{{$user_data['email']}}" placeholder="Enter Email">

                <label for="contact">Contact</label>
                <input type="number" name="contact" class="form-control" value="{{$user_data['contact']}}" placeholder="Enter contact">
                
                <input type="submit" value="Add Details" class="btn btn-success d-block mx-auto mt-4">
            </form>
        </div>
    </div>
</div>

@endsection


@section('footer')

@parent

@endsection