<body>

    @extends('layout.header_footer')

    @section('navbar')

    @parent

    @endsection


    @section('main_content')
    <div class="container mb-4">
        <div class="row">
            <div class="col-md-6 mx-auto bg-dark p-5 text-white">
                @if($errors->any())
                <div class="alert alert-danger">
                    <ul class="list-group">
                        @foreach($errors->all() as $error)
                        <li class="list-group-item">
                            {{$error}}
                        </li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <form action="{{url('details_send')}}" method="post" enctype="multipart/form-data">
                    @csrf()
                    <label for="email">Email</label>
                    <input type="email" name="email" class="form-control" value="" placeholder="Enter Email" id="email">
                    <input type="hidden" name="provider_user_id" class="form-control" readonly>
                    <div id="message" class="rounded p-2 mt-1"></div>
                    
                    <input type="submit" value="Add Details" class="btn btn-success d-block mx-auto mt-4">
                </form>
                <a href="{{url('/redirect')}}">
                    <input type="button" id="submit" name="submit" value="Login With Google" class="btn btn-success d-block mx-auto mt-4">
                </a>
            </div>
        </div>
    </div>

    <script>
        $(function() {
            $('#message').hide();
            $('#email').keyup(function() {
                let email = $(this).val();
                $.ajax({
                    url: "{{url('email_check')}}",
                    type: 'get',
                    data: {
                        email: email
                    },
                    success: function(data) {
                        if (data == 'new_email') {
                            console.log(data);
                            // $('#message').show();
                            // $('#message').addClass('alert-success').removeClass('alert-danger');
                            // $('#message').html('You can go ahead with this email');
                        } else {
                            console.log(data);

                            // $('#message').show();
                            // $('#message').addClass('alert-danger').removeClass('alert-success');
                            // $('#message').html('Email is exists');
                        }
                    }
                });
            });
        });
    </script>
    @endsection

    @section('footer')

    @parent

    @endsection


</body>

</html>