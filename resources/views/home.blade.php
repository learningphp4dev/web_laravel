<body>

    @extends('layout.header_footer')

    @section('navbar')

    @parent

    @endsection


    @section('main_content')
    <div class="container mb-4">
        <div class="row">
            <div class="col-md-6 mx-auto bg-dark p-5 text-white">
                @if($errors->any())
                <div class="alert alert-danger">
                    <ul class="list-group">
                        @foreach($errors->all() as $error)
                        <li class="list-group-item">
                            {{$error}}
                        </li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <form action="{{url('register_complete')}}" method="post" enctype="multipart/form-data">
                    @csrf()
                    <label for="username">Username</label>
                    <input type="text" name="username" class="form-control" placeholder="Enter username">
                    <label for="email">Email</label>
                    <input type="email" name="email" class="form-control" value="@if(Session::has('google_email')) {{Session::get('google_email')}} @endif" placeholder="Enter Email" id="email" readonly>
                    <input type="hidden" name="provider_user_id" class="form-control" value="@if(Session::has('provider_user_id')) {{Session::get('provider_user_id')}} @endif" placeholder="Enter Email" id="email" readonly>
                    <div id="message" class="rounded p-2 mt-1"></div>
                    <label for="contact">Contact</label>
                    <input type="number" name="contact" class="form-control" placeholder="Enter contact">
                    <label for="password">Password</label>
                    <input type="number" name="password" class="form-control" placeholder="Enter contact">
                    <label for="image">Your Profile Picture</label>
                    <input type="file" name="image" class="form-control">
                    <label for="image">Course Video</label>
                    <input type="file" name="video" class="form-control">
                    <label for="image">Select Yout Type</label>
                    <select name="user_type" id="" class="form-control">
                        <option value="user">User</option>
                        <option value="user">Instructor</option>
                    </select>
                    <input type="submit" value="Add Details" class="btn btn-success d-block mx-auto mt-4">
                </form>
            </div>
        </div>
    </div>

   
    @endsection

    @section('footer')

    @parent

    @endsection


</body>

</html>