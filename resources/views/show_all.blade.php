<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>All user data</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
</head>

<body>
    @extends('layout.header_footer')

    @section('navbar')
    @parent
    @endsection

    @section('main_content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 mx-auto">
                @if(session()->has('success'))
                <div class="alert-success p-3 rounded">
                    {{Session::get('success')}}
                </div>
                @endif

                @if(session()->has('deleted'))
                <div class="alert-danger p-3 my-3">
                    {{session()->get('deleted')}}
                </div>
                @endif
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th>id</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Password</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($get_data as $print_user)
                        <tr>
                            <td>{{$print_user->id}}</td>
                            <td>{{$print_user->username}}</td>
                            <td>{{$print_user->email}}</td>
                            <td>{{$print_user->password}}</td>
                            <td>
                                <a href="{{url('delete',[$print_user->id])}}" class="btn btn-warning">Delete</a>
                                <a href="{{url('/Edit',[$print_user->id])}}" class="btn btn-danger">Edit</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endsection

    @section('footer')
    @parent
    @endsection
</body>

</html>