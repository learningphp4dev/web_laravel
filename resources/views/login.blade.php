@extends('layout.header_footer')

@section('navbar')
@parent
@endsection

@section('main_content')
<!-- Login form -->

<div class="container text-white">
    <div class="row">
        <div class="col-md-6  bg-dark p-4 my-3 rounded mx-auto">
            <!-- this session check syntax is for flash messages -->
            @if(session()->has('fail'))
            <div class="alert-danger p-3 rounded">
                {{Session::get('fail')}}
            </div>
            @endif
            <h1 class="text-center">Login</h1>
            <form action="{{url('login_fn')}}" method="post">
                @csrf()
                <label for="email">Email</label>
                <input type="text" placeholder="Enter your email" name="email" class="form-control">
                <label for="password">Password</label>
                <input type="text" placeholder="Enter your password" name="password" class="form-control">

                <input type="submit" class="btn btn-danger d-block mx-auto my-2">
            </form>
        </div>
    </div>
</div>
@endsection

@section('footer')
@parent
@endsection