<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>All user data</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
</head>

<body>
    @extends('layout.header_footer')

    @section('navbar')
    @parent
    @endsection

    @section('main_content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 alert-success mt-4 mb-4 p-2 rounded">
                <div class="image" height="200" width="200" style="overflow: hidden;">
                    <img src='{{asset("profile_image/$user_data->image")}}' height="200" width="200" class="img-fluid" alt="">
                </div>
                <h3 class="display-4">Welcome Back, {{$user_data->username}}</h3>
                <h3>Your contact is :- {{$user_data->contact}}</h3>
                <h3>Your password is :- {{$user_data->password}}</h3>
            </div>
            <div class="col-md-6 alert-info mt-4 mb-4 p-2 rounded">
                <div class="image" height="200" width="200" style="overflow: hidden;">
                    <video src='{{asset("video/$user_data->video")}}' height="400" width="100%" type = "video/mp4" controls></video>
                </div>
            </div>
        </div>
    </div>
    @endsection

    @section('footer')
    @parent
    @endsection
</body>

</html>