<?php

namespace App\Services;

use App\socialAuthGooglemodel;
use App\user_profile;
use Session;
use Laravel\Socialite\Contracts\User as ProviderUser;

class SocialGoogleAccountService
{
    public function createOrGetUser(ProviderUser $providerUser)
    {
        $account = socialAuthGooglemodel::whereProvider('google')
            ->whereProviderUserId($providerUser->getId())
            ->first();
            // return 'Returning an Account:- '.$account;
        if ($account) {
            // session()->flash('you already registered');
            // return redirect('login');
            // echo '<pre>';
            // echo 'in services account if:-';
            // print_r($account);
            // die();
            return $account->user;
        } else {
            // if google auth model has no same click email then it will come here aqnd fill the google model
            $account = new socialAuthGooglemodel([
                'provider_user_id' => $providerUser->getId(),
                'provider' => 'google'
            ]);
            $user = user_profile::whereEmail($providerUser->getEmail())->first();
            if (!$user) {
                $user = user_profile::create([
                    'provider_user_id' => $providerUser->getId(),
                    'email' => $providerUser->getEmail(),
                ]);
            }
            $account->user()->associate($user);
            $account->save();
            return $user;
        }
    }
}
