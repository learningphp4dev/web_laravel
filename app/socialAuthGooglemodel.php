<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class socialAuthGooglemodel extends Model
{
    public $fillable = ['provider_user_id','user_id','provider'];

    public function user(){
        return $this->belongsTo(user_profile::class);
    }
}
