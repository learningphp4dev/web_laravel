<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Session;
// this is use for database connection as same as we use $connect in xampp
use Illuminate\Support\Facades\DB;

// this is model we include model in it and perform crud using model it's called Eloquent
use App\user_profile;

class user_profile_cont extends Controller
{
    public function index()
    {
        $Username = 'My name is Jaspreet';
        return view('register')->with('MeraNaam', $Username);
    }

    public function store(Request $request)
    {
        $check = user_profile::where('email', $request->email)->exists();
        if ($check == $request->email) {
            Session::put('google_email', $request->email);
            Session::put('provider_user_id', $request->provider_user_id);
            return redirect('register_complete_page');
        }
        // Making object of model class
        $insert_data = new user_profile;

        // request all the data and save it in variable
        $request_data = request()->all();

        // intialize all form data into object
        $insert_data->email = $request_data['email'];
        $number_of_digits = 30;
        $rand_number =  substr(number_format(time() * mt_rand(), 0, '', ''), 0, $number_of_digits);
        $insert_data->provider_user_id =  $rand_number;
        $insert_data->save();

        Session::put('google_email', $request_data['email']);
        Session::put('provider_user_id', $insert_data->provider_user_id);
        return redirect('register_complete_page');
    }

    public function show()
    {
        $select = user_profile::all();
        session()->flash('success', 'You login Succesfully');
        return view('show_all')->with('get_data', $select);
    }

    public function delete($id)
    {
        user_profile::where('id', $id)->first()->delete();
        return redirect(url('fetch'));
    }

    public function edit($id)
    {
        $edit = user_profile::find($id);
        return view('edit_user')->with('user_data', $edit);
    }

    public function update_fn(Request $request)
    {
        $table = user_profile::where('email', $request->email)->first();

        $table->username = $request->username;
        $table->email = $request->email;
        $table->contact = $request->contact;

        $table->save();

        redirect(url('fetch'));
    }

    public function login_form()
    {
        return view('login');
    }

    public function login(Request $request)
    {
        $login_query = user_profile::where('email', $request->email)->where('password', $request->password)->first();

        // echo '<pre>';
        // print_r($login_query);
        // die();
        if ($login_query) {
            Session::put('logged_in', $login_query['email']);
            session()->flash('success', 'You login successfully');
            return redirect('/fetch');
        } else {
            session()->flash('fail', 'You credential is not correct');
            return redirect('/login');
        }
    }

    public function logout()
    {
        Session::flush();
        return redirect('login');
    }

    // User Dashboard
    public function user()
    {
        $query = user_profile::where('email', Session::get('logged_in'))->first();

        return view('user_dashboard')->with('user_data', $query);
    }

    // check email exist or not
    public function check_email(Request $request)
    {
        $check = user_profile::where('email', $request->email)->exists();
        // echo '<pre>';
        // print_r($check);
        // die();
        if (!$check) return 'new_email';
        return 'exist';
    }

    public function register_complete(Request $request)
    {
        $this->validate(request(), [
            "image" => 'image|mimes:jpeg,png,webp,jpg,pdf|max:5120',
            // "video" => 'video|max:102400',
        ]);
        // echo '<pre>';
        // print_r($request->all());
        // die();
        $insert_data = user_profile::where('email', $request->email)->first();
        global $image_name;
        global $video_name;
        $insert_data->username = $request->username;
        $insert_data->contact = $request->contact;
        $insert_data->password  = $request->password;
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $image_name = $image->getClientOriginalName();
            $image->move(public_path() . '/profile_image/', $image_name);
        }

        if ($request->hasFile('video')) {
            $video = $request->file('video');
            $video_name = $video->getClientOriginalName();
            $video->move(public_path().'/video/', $video_name);
        }


        $mail_array = array(
            'username' => $request['username'],
            'contact' => $request['contact'],
            'email' => $request['email'],
        );
        Mail::send('email_view.register_email', $mail_array, function ($m) use ($mail_array) {
            // echo '<pre>';
            // print_r($m['email']);
            // die();
            $m->to($mail_array['email'])->subject('Bloodshed Gaming');
            $m->from('Bloodshed@gmail.com', 'Your Registeration is complete');
        });

        // echo $image_name;
        // die();
        $insert_data->image = $image_name;
        $insert_data->video = $video_name;
        $insert_data->user_type = $request->user_type;
        Session::flush();
        $insert_data->save();
        // saved it in backend database using eloquent method
        return redirect(url('/login'));
    }
}
