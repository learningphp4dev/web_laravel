<?php

namespace App\Http\Controllers;

use App\SocialAuthGooglemodel;
use Illuminate\Http\Request;
use Socialite;
use Session;
use App\user_profile;
use App\Services\SocialGoogleAccountService;


class SocialAuthGoogleControllerController extends Controller
{
    /**
     * Create a redirect method to google api.
     *
     * @return void
     */
    public function redirect()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Return a callback method from google api.
     *
     * @return callback URL from google
     */
    public function callback(SocialGoogleAccountService $service)
    {
        //  Socialite::driver('google')   ->    It's return the data which provided by google_provider
        //  Socialite::driver('google')->user();   -> It's return the data which provided by google particular user info
        // $check = Socialite::driver('google')->user();
        // echo '<pre>';
        // var_dump($check);
        // echo 'Controller';
        // die();
        $user = $service->createOrGetUser(Socialite::driver('google')->user());
        // echo '<pre>'; 
        // echo 'in call back function';
        // print_r($user);
        // die();
        Session::put('complete_email',$user->email);
        Session::put('provider_user_id',$user->provider_user_id);
        // return redirect('register_complete_page');
        return 'controller '.$user; //when everything goes fine then redirect the user to complete registration
    }
}
